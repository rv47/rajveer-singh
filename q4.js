var teststring = 'abcdedcba';

function IsPalindromeIteration(str)
{
	var len = str.length, i=0, result = true;
	if (len <= 1) return true;
	while(i != len - i - 1)
  {
		var start = str.charAt(i),
	  end = str.charAt(len - i - 1);
		if (start != end)
    {
			return false;
		}
		i++;
	}
	return result;
}

// res will have a value 'true'
var res = IsPalindromeIteration(teststring);
console.log( res);
